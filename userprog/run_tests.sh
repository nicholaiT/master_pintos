
# cd into build to run test cases
cd ./build;

# create a filesystem with 2 MB for duration of pintos run
pintos-mkdisk filesys.dsk --filesys-size=2;
pintos -f -q
pintos -p ../../examples/echo -a echo -- -q
pintos -p ../../examples/custom_test -a custom_test -- -q
pintos --gdb -v -- -q run "custom_test"