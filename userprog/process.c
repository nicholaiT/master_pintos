#include "userprog/process.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/tss.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "threads/flags.h"
#include "threads/init.h"
#include "threads/interrupt.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"

static thread_func start_process NO_RETURN;
static bool load(const char *cmdline,
                 char *file_name, void (**eip)(void), void **esp);

////////////////////////////////////////////////////////////////////////////////
/* Prototypes added by group */
struct child_thread *insert_child_into_parent();
void wake_up_parent();
static int get_argc(char *cmdline);
static bool allocate_stack_page(uint8_t *kpage);
////////////////////////////////////////////////////////////////////////////////

/* Starts a new thread running a user program loaded from
   FILENAME.  The new thread may be scheduled (and may even exit)
   before process_execute() returns.  Returns the new process's
   thread id, or TID_ERROR if the thread cannot be created. 
   
   Nicholai && Chris && Raoul drove */
tid_t process_execute(const char *file_name)
{
    char *fn_copy;
    tid_t tid;
    char *cmd_args;
    struct thread *cur = thread_current();

    /* Make a copy of FILE_NAME.
     Otherwise there's a race between the caller and load(). */
    fn_copy = palloc_get_page(0);
    if (fn_copy == NULL)
        return TID_ERROR;
    strlcpy(fn_copy, file_name, PGSIZE);

    /* Use a temp string to get the executable name */
    char temp[1024];
    strlcpy(temp, file_name, strlen(file_name) + 1);
    ASSERT(temp != NULL);
    char *exe_name = strtok_r(temp, " ", &cmd_args);

    /* Use group defined struct to store name, cmdline, and the parent thread */
    process_metadata *process_struct =
        (process_metadata *)malloc(sizeof(process_metadata));

    /* Error check the malloc call */
    if (process_struct == NULL)
    {
        printf("[process_execute] - malloc error\n");
        return TID_ERROR;
    }
    process_struct->cmdline = fn_copy;
    process_struct->exe_name = exe_name;
    process_struct->parent_thread = cur;

    /* Create a new thread to execute exe_name. */
    tid = thread_create(exe_name, PRI_DEFAULT, start_process, process_struct);

    /* Block until child has been loaded */
    sema_down(&cur->exec_sema);

    /* Set tid to -1 if load failed */
    if (!cur->child_load_success)
        tid = TID_ERROR;
    else
        insert_child_into_parent(tid);

    /* Free the cmdline copy */
    if (tid == TID_ERROR)
        palloc_free_page(fn_copy);

    return tid;
}

/* A thread function that loads a user process and starts it
   running. 
   
   Chris && Raoul && Nicholai drove */
static void
start_process(void *process_struct_)
{
    /* Get the metadata of the new process */
    process_metadata *process_info = process_struct_;
    char *file_name = process_info->exe_name;
    char *cmdline = process_info->cmdline;
    struct thread *parent_thread = process_info->parent_thread;
    struct intr_frame if_;
    bool success;

    /* Initialize interrupt frame and load executable. */
    memset(&if_, 0, sizeof if_);
    if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
    if_.cs = SEL_UCSEG;
    if_.eflags = FLAG_IF | FLAG_MBS;

    /* Load the new executable file and setup the child process's data*/
    success = load(cmdline, file_name, &if_.eip, &if_.esp);

    /* Free the process's metadata */
    free(process_info);

    /* If load failed, quit and signal the parent */
    if (!success)
    {
        parent_thread->child_load_success = false;
        sema_up(&parent_thread->exec_sema);
        thread_exit();
    }

    /* Set a reference to the parent thread */
    thread_current()->parent_thread = parent_thread;

    /* Free the copy of the cmdline */
    palloc_free_page(cmdline);

    /* Signal to parent the child has loaded correctly */
    sema_up(&parent_thread->exec_sema);

    /* Start the user process by simulating a return from an
     interrupt, implemented by intr_exit (in
     threads/intr-stubs.S).  Because intr_exit takes all of its
     arguments on the stack in the form of a `struct intr_frame',
     we just point the stack pointer (%esp) to our stack frame
     and jump to it. */
    asm volatile("movl %0, %%esp; jmp intr_exit"
                 :
                 : "g"(&if_)
                 : "memory");

    NOT_REACHED();
}

/* Waits for thread TID to die and returns its exit status.  If
   it was terminated by the kernel (i.e. killed due to an
   exception), returns -1.  If TID is invalid or if it was not a
   child of the calling process, or if process_wait() has already
   been successfully called for the given TID, returns -1
   immediately, without waiting.

   This function will be implemented in problem 2-2.  For now, it
   does nothing. 
   
   Nicholai && Chris && Raoul drove
   */
int process_wait(tid_t child_tid)
{
    /* Insure only one thread is accessing the current parent's 
    children's list */
    struct semaphore *children_list_sema =
        &thread_current()->children_list_sema;
    sema_down(children_list_sema);

    /* Sequential search for the child that the parent is waiting on */
    struct thread *cur_child_thread = NULL;
    struct list *children = &thread_current()->children;
    struct list_elem *e = NULL;
    struct child_thread *cur_child = NULL;
    for (e = list_begin(children); e != list_end(children); e = list_next(e))
    {
        cur_child = list_entry(e, struct child_thread, child_elem);

        if (cur_child->child_id == child_tid)
        {
            cur_child_thread = cur_child->child_thread;
            break;
        }
    }
    sema_up(children_list_sema);

    /* Child doesn't exist in parent's children list */
    if (cur_child_thread == NULL)
    {
        return -1;
    }

    /* Remove the child from the children list */
    list_remove(e);

    /* Tell child that parent is ready for exit status */
    sema_up(&cur_child_thread->exit_status_sema);

    /* Wait on the child to finish executing */
    sema_down(&cur_child_thread->child_wait_sema);
    int exit_status = thread_current()->child_exit_status;

    /* Free the memory of the child_thread struct */
    free(cur_child);
    cur_child = NULL;

    return exit_status;
}

/* Free the current process's resources. */
void process_exit(void)
{
    struct thread *cur = thread_current();
    uint32_t *pd = cur->pagedir;

    int i;
    for (i = 2; i < MAX_FILE_DESCRIPTORS; i++)
    {
        if (cur->file_descriptors[i] != NULL)
        {
            file_close(cur->file_descriptors[i]);
            cur->file_descriptors[i] = NULL;
        }
    }

    struct semaphore *children_list_sema =
        &cur->children_list_sema;
    sema_down(children_list_sema);

    struct thread *cur_child_thread = NULL;
    struct list *children = &cur->children;
    struct list_elem *e = NULL;

    for (e = list_begin(children); e != list_end(children);)
    {
        struct child_thread *cur_child = 
            list_entry(e, struct child_thread, child_elem);
        //sema_up(&cur_child->child_thread->exit_status_sema);
        //cur_child->child_thread->parent_thread = NULL;
        e = list_remove(e);
        free(cur_child);
    }
    sema_up(children_list_sema);

    /* Wake up the parent of the exiting thread */
    if (cur->parent_thread != NULL)
    {
        wake_up_parent();
    }

    /* Destroy the current process's page directory and switch back
     to the kernel-only page directory. */
    if (pd != NULL)
    {
        /* Correct ordering here is crucial.  We must set
         cur->pagedir to NULL before switching page directories,
         so that a timer interrupt can't switch back to the
         process page directory.  We must activate the base page
         directory before destroying the process's page
         directory, or our active page directory will be one
         that's been freed (and cleared). */
        cur->pagedir = NULL;
        pagedir_activate(NULL);
        pagedir_destroy(pd);
    }
}

/* Sets up the CPU for running user code in the current
   thread.
   This function is called on every context switch. */
void process_activate(void)
{
    struct thread *t = thread_current();

    /* Activate thread's page tables. */
    pagedir_activate(t->pagedir);

    /* Set thread's kernel stack for use in processing
     interrupts. */
    tss_update();
}

/* We load ELF binaries.  The following definitions are taken
   from the ELF specification, [ELF1], more-or-less verbatim.  */

/* ELF types.  See [ELF1] 1-2. */
typedef uint32_t Elf32_Word, Elf32_Addr, Elf32_Off;
typedef uint16_t Elf32_Half;

/* For use with ELF types in printf(). */
#define PE32Wx PRIx32 /* Print Elf32_Word in hexadecimal. */
#define PE32Ax PRIx32 /* Print Elf32_Addr in hexadecimal. */
#define PE32Ox PRIx32 /* Print Elf32_Off in hexadecimal. */
#define PE32Hx PRIx16 /* Print Elf32_Half in hexadecimal. */

/* Executable header.  See [ELF1] 1-4 to 1-8.
   This appears at the very beginning of an ELF binary. */
struct Elf32_Ehdr
{
    unsigned char e_ident[16];
    Elf32_Half e_type;
    Elf32_Half e_machine;
    Elf32_Word e_version;
    Elf32_Addr e_entry;
    Elf32_Off e_phoff;
    Elf32_Off e_shoff;
    Elf32_Word e_flags;
    Elf32_Half e_ehsize;
    Elf32_Half e_phentsize;
    Elf32_Half e_phnum;
    Elf32_Half e_shentsize;
    Elf32_Half e_shnum;
    Elf32_Half e_shstrndx;
};

/* Program header.  See [ELF1] 2-2 to 2-4.
   There are e_phnum of these, starting at file offset e_phoff
   (see [ELF1] 1-6). */
struct Elf32_Phdr
{
    Elf32_Word p_type;
    Elf32_Off p_offset;
    Elf32_Addr p_vaddr;
    Elf32_Addr p_paddr;
    Elf32_Word p_filesz;
    Elf32_Word p_memsz;
    Elf32_Word p_flags;
    Elf32_Word p_align;
};

/* Values for p_type.  See [ELF1] 2-3. */
#define PT_NULL 0           /* Ignore. */
#define PT_LOAD 1           /* Loadable segment. */
#define PT_DYNAMIC 2        /* Dynamic linking info. */
#define PT_INTERP 3         /* Name of dynamic loader. */
#define PT_NOTE 4           /* Auxiliary info. */
#define PT_SHLIB 5          /* Reserved. */
#define PT_PHDR 6           /* Program header table. */
#define PT_STACK 0x6474e551 /* Stack segment. */

/* Flags for p_flags.  See [ELF3] 2-3 and 2-4. */
#define PF_X 1 /* Executable. */
#define PF_W 2 /* Writable. */
#define PF_R 4 /* Readable. */

static bool setup_stack(void **esp, char *arg_ptr);
static bool validate_segment(const struct Elf32_Phdr *, struct file *);
static bool load_segment(struct file *file, off_t ofs, uint8_t *upage,
                         uint32_t read_bytes, uint32_t zero_bytes,
                         bool writable);

/* Loads an ELF executable from FILE_NAME into the current thread.
   Stores the executable's entry point into *EIP
   and its initial stack pointer into *ESP.
   Returns true if successful, false otherwise. */
bool load(const char *cmdline, char *file_name, void (**eip)(void), void **esp)
{
    struct thread *t = thread_current();
    struct Elf32_Ehdr ehdr;
    struct file *file = NULL;
    off_t file_ofs;
    bool success = false;
    int i;

    /* Allocate and activate page directory. */
    t->pagedir = pagedir_create();
    if (t->pagedir == NULL)
        goto done;
    process_activate();

    /* Open executable file. */
    file = filesys_open(file_name);

    if (file == NULL)
    {
        printf("load: %s: open failed\n", file_name);
        goto done;
    }

    file_deny_write(file);
    thread_current()->running_exe = file;

    /* Read and verify executable header. */
    if (file_read(file, &ehdr, sizeof ehdr) != sizeof ehdr ||
        memcmp(ehdr.e_ident, "\177ELF\1\1\1", 7) ||
        ehdr.e_type != 2 || ehdr.e_machine != 3 ||
        ehdr.e_version != 1 ||
        ehdr.e_phentsize != sizeof(struct Elf32_Phdr) ||
        ehdr.e_phnum > 1024)
    {
        printf("load: %s: error loading executable\n", file_name);
        goto done;
    }

    /* Read program headers. */
    file_ofs = ehdr.e_phoff;
    for (i = 0; i < ehdr.e_phnum; i++)
    {
        struct Elf32_Phdr phdr;

        if (file_ofs < 0 || file_ofs > file_length(file))
            goto done;
        file_seek(file, file_ofs);

        if (file_read(file, &phdr, sizeof phdr) != sizeof phdr)
            goto done;
        file_ofs += sizeof phdr;
        switch (phdr.p_type)
        {
        case PT_NULL:
        case PT_NOTE:
        case PT_PHDR:
        case PT_STACK:
        default:
            /* Ignore this segment. */
            break;
        case PT_DYNAMIC:
        case PT_INTERP:
        case PT_SHLIB:
            goto done;
        case PT_LOAD:
            if (validate_segment(&phdr, file))
            {
                bool writable = (phdr.p_flags & PF_W) != 0;
                uint32_t file_page = phdr.p_offset & ~PGMASK;
                uint32_t mem_page = phdr.p_vaddr & ~PGMASK;
                uint32_t page_offset = phdr.p_vaddr & PGMASK;
                uint32_t read_bytes, zero_bytes;
                if (phdr.p_filesz > 0)
                {
                    /* Normal segment.
                     Read initial part from disk and zero the rest. */
                    read_bytes = page_offset + phdr.p_filesz;
                    zero_bytes =
                        (ROUND_UP(page_offset + phdr.p_memsz, PGSIZE) - read_bytes);
                }
                else
                {
                    /* Entirely zero.
                     Don't read anything from disk. */
                    read_bytes = 0;
                    zero_bytes = ROUND_UP(page_offset + phdr.p_memsz, PGSIZE);
                }
                if (!load_segment(file, file_page, (void *)mem_page,
                                  read_bytes, zero_bytes, writable))
                    goto done;
            }
            else
                goto done;
            break;
        }
    }

    /* Set up stack. */
    if (!setup_stack(esp, cmdline))
        goto done;

    /* Start address. */
    *eip = (void (*)(void))ehdr.e_entry;

    success = true;

done:
    /* We arrive here whether the load is successful or not. */
    return success;
}

/* load() helpers. */

static bool install_page(void *upage, void *kpage, bool writable);

/* Checks whether PHDR describes a valid, loadable segment in
   FILE and returns true if so, false otherwise. */
static bool
validate_segment(const struct Elf32_Phdr *phdr, struct file *file)
{
    /* p_offset and p_vaddr must have the same page offset. */
    if ((phdr->p_offset & PGMASK) != (phdr->p_vaddr & PGMASK))
        return false;

    /* p_offset must point within FILE. */
    if (phdr->p_offset > (Elf32_Off)file_length(file))
        return false;

    /* p_memsz must be at least as big as p_filesz. */
    if (phdr->p_memsz < phdr->p_filesz)
        return false;

    /* The segment must not be empty. */
    if (phdr->p_memsz == 0)
        return false;

    /* The virtual memory region must both start and end within the
     user address space range. */
    if (!is_user_vaddr((void *)phdr->p_vaddr))
        return false;
    if (!is_user_vaddr((void *)(phdr->p_vaddr + phdr->p_memsz)))
        return false;

    /* The region cannot "wrap around" across the kernel virtual
     address space. */
    if (phdr->p_vaddr + phdr->p_memsz < phdr->p_vaddr)
        return false;

    /* Disallow mapping page 0.
     Not only is it a bad idea to map page 0, but if we allowed
     it then user code that passed a null pointer to system calls
     could quite likely panic the kernel by way of null pointer
     assertions in memcpy(), etc. */
    if (phdr->p_vaddr < PGSIZE)
        return false;

    /* It's okay. */
    return true;
}

/* Loads a segment starting at offset OFS in FILE at address
   UPAGE.  In total, READ_BYTES + ZERO_BYTES bytes of virtual
   memory are initialized, as follows:

        - READ_BYTES bytes at UPAGE must be read from FILE
          starting at offset OFS.

        - ZERO_BYTES bytes at UPAGE + READ_BYTES must be zeroed.

   The pages initialized by this function must be writable by the
   user process if WRITABLE is true, read-only otherwise.

   Return true if successful, false if a memory allocation error
   or disk read error occurs. */
static bool
load_segment(struct file *file, off_t ofs, uint8_t *upage,
             uint32_t read_bytes, uint32_t zero_bytes, bool writable)
{
    ASSERT((read_bytes + zero_bytes) % PGSIZE == 0);
    ASSERT(pg_ofs(upage) == 0);
    ASSERT(ofs % PGSIZE == 0);

    file_seek(file, ofs);
    while (read_bytes > 0 || zero_bytes > 0)
    {
        /* Calculate how to fill this page.
         We will read PAGE_READ_BYTES bytes from FILE
         and zero the final PAGE_ZERO_BYTES bytes. */
        size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
        size_t page_zero_bytes = PGSIZE - page_read_bytes;

        /* Get a page of memory. */
        uint8_t *kpage = palloc_get_page(PAL_USER);
        if (kpage == NULL)
            return false;

        /* Load this page. */
        if (file_read(file, kpage, page_read_bytes) != (int)page_read_bytes)
        {
            palloc_free_page(kpage);
            return false;
        }
        memset(kpage + page_read_bytes, 0, page_zero_bytes);

        /* Add the page to the process's address space. */
        if (!install_page(upage, kpage, writable))
        {
            palloc_free_page(kpage);
            return false;
        }

        /* Advance. */
        read_bytes -= page_read_bytes;
        zero_bytes -= page_zero_bytes;
        upage += PGSIZE;
    }
    return true;
}

/* Create a minimal stack by mapping a zeroed page at the top of
   user virtual memory. 
   
   Raoul && Nicholai && Chris drove */
static bool
setup_stack(void **esp_ref, char *cmdline)
{
    uint8_t *kpage;
    bool success = false;
    char *esp = PHYS_BASE;

    success = allocate_stack_page(kpage);

    /* When allocation or installation fails*/
    if (!success)
        return success;

    // Raoul drove

    /* Initialize argc and argv */
    int argc = get_argc(cmdline);

    /* Too many arguments */
    if (argc > 128)
    {
        palloc_free_page(kpage);
        return false;
    }

    /* Restrict to 128 arguments */
    char *argv[128];

    int i = 0;
    int cmdlength = 0;
    char *cur_arg, *save_ptr;

    /* Tokenize the cmdline and add the arguments to argv */
    for (cur_arg = strtok_r(cmdline, " ", &save_ptr); cur_arg != NULL;
         cur_arg = strtok_r(NULL, " ", &save_ptr))
    {
        argv[i] = cur_arg;
        cmdlength += (strlen(cur_arg) + 1); // Add one extra for sentinel
        i++;
    }

    /* Move the stack pointer to account for all cmdline arguments */
    esp = esp - cmdlength;

    /* Error check to make sure all arguments were added */
    if (i != argc)
    {
        printf("[setup_stack] - Error initializing argv");
        palloc_free_page(kpage);
        return false;
    }

    /* Initialize the sentinel pointer */
    argv[argc] = NULL;

    // Nicholai drove

    char *cur_word_start = esp;

    /* Push the elements of argv onto the stack by going forward */
    for (i = 0; i < argc; i++)
    {
        strlcpy(cur_word_start, argv[i], strlen(argv[i]) + 1);
        argv[i] = cur_word_start;
        cur_word_start = cur_word_start + strlen(argv[i]) + 1;
    }

    /* Handle any byte padding if cmdlength is not divisible by sizeof(*) */
    if (cmdlength % sizeof(char *) > 0)
    {
        int padding = 4 - (cmdlength % 4);
        esp -= padding;
    }

    /* Point esp to the address of argv */
    esp -= (sizeof(char *) * (argc + 1));

    /* Add the pointers to the argument strings from argv by going forward 
       Casting esp into a double pointer allows us to ... */
    char **cur_stack_address;
    for (i = 0; i <= argc; i++)
    {
        char *address = (char *)argv[i];
        cur_stack_address = esp + (i * sizeof(char *));
        *cur_stack_address = address;
    }

    // Chris drove

    /* Push argv on to the stack by actually pushing the reference to argv[0] */
    esp -= sizeof(char *);
    cur_stack_address = esp;
    *cur_stack_address = (esp + sizeof(char *));

    /* Push argc onto stack */
    esp -= sizeof(int);
    *esp = argc;

    /* Push a fake return address */
    esp -= 4;
    cur_stack_address = esp;
    *cur_stack_address = NULL;

    /* Make sure arguments don't overload the stack*/
    if (PHYS_BASE - (void *)esp > PGSIZE)
    {
        palloc_free_page(kpage);
        return false;
    }

    /* reset esp and return */
    *esp_ref = esp;
    return success;
}

/* Adds a mapping from user virtual address UPAGE to kernel
   virtual address KPAGE to the page table.
   If WRITABLE is true, the user process may modify the page;
   otherwise, it is read-only.
   UPAGE must not already be mapped.
   KPAGE should probably be a page obtained from the user pool
   with palloc_get_page().
   Returns true on success, false if UPAGE is already mapped or
   if memory allocation fails. */
static bool
install_page(void *upage, void *kpage, bool writable)
{
    struct thread *t = thread_current();

    /* Verify that there's not already a page at that virtual
     address, then map our page there. */
    return (pagedir_get_page(t->pagedir, upage) == NULL &&
            pagedir_set_page(t->pagedir, upage, kpage, writable));
}

////////////////////////////////////////////////////////////////////////////////
/* Helper functions added by group */

/* Nicholai && Chris drove

   Allocates the stack page for the user program. 
   Performs error check to insure page allocation and installation
   was successful. */
static bool
allocate_stack_page(uint8_t *kpage)
{
    bool success = false;
    /* Allocate page of memory for user stack */
    kpage = palloc_get_page(PAL_USER | PAL_ZERO);

    /* Return failure, no more pages */
    if (kpage == NULL)
    {
        return success;
    }

    success = install_page(((uint8_t *)PHYS_BASE) - PGSIZE, kpage, true);

    /* If mapping fails, free the page and return */
    if (!success)
    {
        palloc_free_page(kpage);
    }

    return success;
}

/* Nicholai and Raoul drove

  Returns # of arguments in a given cmdline */
static int
get_argc(char *cmdline)
{

    /* Use a temp string to get the executable name */
    char temp[1024];
    strlcpy(temp, cmdline, strlen(cmdline) + 1);
    ASSERT(temp != NULL && cmdline != NULL);

    char *cur_arg;
    char *save_ptr;
    int argc = 0;

    /* Loop until no more tokens, return # of tokens */
    for (cur_arg = strtok_r(temp, " ", &save_ptr); cur_arg != NULL;
         cur_arg = strtok_r(NULL, " ", &save_ptr))
    {
        argc++;
    }

    return argc;
}

/* Nicholai && Chris drove

  Wait for the parent to be ready to receive the child's exit status, and then 
  give the parent the child's exit status when the parent is ready and 
  then wake the parent up. */
void wake_up_parent()
{
    struct thread *cur = thread_current();
    struct thread *parent_thread = cur->parent_thread;

    /* Wait until parent is ready for the exit status */
    sema_down(&cur->exit_status_sema);

    /* Store the child's exit status within the parent thread */
    parent_thread->child_exit_status = cur->exit_status;

    /* Tell the parent that the child has finished executing */
    sema_up(&cur->child_wait_sema);
}

/* Nicholai && Raoul && Chris drove
  
  Add newly created child process into parent's children list. */
struct child_thread *
insert_child_into_parent(int tid)
{
    struct thread *cur = get_thread_by_tid(tid);
    struct thread *parent_thread = thread_current();

    struct child_thread *new_child =
        (struct child_thread *)malloc(sizeof(struct child_thread));

    /* Error check the malloc call */
    if (new_child == NULL)
    {
        printf("[insert_child_into_parent] - malloc error\n");
        custom_exit(-1);
    }

    new_child->child_thread = cur;
    new_child->child_id = cur->tid;

    /* Safely grab parent's children list */
    struct semaphore *children_list_sema = &parent_thread->children_list_sema;

    sema_down(children_list_sema);
    /* Add the new child_thread struct into the parent's children list */
    list_push_back(&parent_thread->children, &new_child->child_elem);
    sema_up(children_list_sema);

    return new_child;
}
