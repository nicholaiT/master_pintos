#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"
#include "lib/kernel/list.h"

tid_t process_execute(const char *file_name);
int process_wait(tid_t);
void process_exit(void);
void process_activate(void);

////////////////////////////////////////////////////////////////////////////////
/* Struct added by group */

typedef struct process_metadata
{
    char *cmdline;
    char *exe_name;
    struct thread *parent_thread;
} process_metadata;
////////////////////////////////////////////////////////////////////////////////

#endif /* userprog/process.h */
