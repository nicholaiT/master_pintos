#include "userprog/syscall.h"
#include <stdio.h>
#include <stdbool.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/synch.h"

////////////////////////////////////////////////////////////////////////////////
/* Global variables, #define, and prototypes added by group */
#define OUT_BATCH_SIZE 100   /* Write only OUT_BATCH_SIZE bytes at a time*/

/* Array of system calls that take one argument */
int one_arg[10] = {
    SYS_EXIT,
    SYS_EXEC,
    SYS_WAIT,
    SYS_REMOVE,
    SYS_OPEN,
    SYS_FILESIZE,
    SYS_TELL,
    SYS_CLOSE};

/* Array of system calls that take 2 arguments */
int two_args[2] = {
    SYS_CREATE,
    SYS_SEEK};

/* Array of system calls that take 3 arguments */
int three_args[2] = {
    SYS_READ,
    SYS_WRITE};

/* Array of function pointers to the system calls mapped to syscall enum */
void *(*system_calls[NUM_SYS_CALLS])(void *, void *, void *) = {
    halt,
    exit,
    exec,
    wait,
    create,
    remove,
    open,
    filesize,
    read,
    write,
    seek,
    tell,
    close};

/* Global file system lock */
struct lock file_lock;

void check_string_validity(char *s, int size);
void is_fd_in_range(int fd);
void custom_exit(int status);
bool is_in_array(int value, int array[], int size);
////////////////////////////////////////////////////////////////////////////////

/* Prototypes */
static void syscall_handler(struct intr_frame *);

/* Initialize system calls and &file_lock for system */
void 
syscall_init(void)
{
  lock_init(&file_lock);
  intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");
}

/* Nicholai && Chris && Raoul drove 

  Invoke handler once system call was pushed onto the stack */
static void
syscall_handler(struct intr_frame *f UNUSED)
{
  char *stack_ptr = f->esp; /* Get frame's stack pointer */
  uint32_t *pd = thread_current()->pagedir; /* Get process's page dir */
  void *args[] = {NULL, NULL, NULL};  /* Array of args */
  int syscall_num = -1, num_args = 0, i;  /* init syscall_num & # of args */

  /* Checks if address is a valid user virtual address and has been mapped into
    physical memory. */
  if (stack_ptr == NULL || 
    !is_user_vaddr(stack_ptr) ||
    pagedir_get_page(pd, stack_ptr) == NULL)
  {
    custom_exit(-1);
    NOT_REACHED();
  }

  /* Address is now valid and can be looked up by system call handler */
  syscall_num = *(int *)stack_ptr;

  /* Error check to insure an existing system call was called */
  if (syscall_num < 0 || syscall_num > NUM_SYS_CALLS - 1)
  {
    custom_exit(-1);
    NOT_REACHED();
  }

  /* Move the user stack pointer to point at system call arguments */
  stack_ptr += sizeof(char *);

  /* Find out how many arguments the system call needs */
  if (is_in_array(syscall_num, three_args, 2))
    num_args = 3;
  else if (is_in_array(syscall_num, two_args, 2))
    num_args = 2;
  else if (is_in_array(syscall_num, one_arg, 8))
    num_args = 1;

  /* Get the system call arguments off the stack and store them in the args
    array. */
  for (i = 0; i < num_args; i++)
  {
    /* Exit with -1 status if any argument is invalid */
    if (stack_ptr == NULL || 
      !is_user_vaddr(stack_ptr) || 
      pagedir_get_page(pd, stack_ptr) == NULL)
    {
      custom_exit(-1);
      NOT_REACHED();
    }

    args[i] = stack_ptr;
    stack_ptr += sizeof(char *);
  }

  /* Call the appropriate system call and set the return value */
  f->eax = (*system_calls[syscall_num])(args[0], args[1], args[2]);
}

/* Nicholai drove

Shutdown pintos when called. */
void 
halt(void)
{
  shutdown_power_off();
  NOT_REACHED();
}

/* Nicholai drove 

  Gets the exit status and sends the status to custom_exit(). */
void 
exit(void *status_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  int status = *(int *)status_;

  /* Send status to custom_exit() to deallocate process */
  custom_exit(status);
  NOT_REACHED();
}

/* Nicholai && Chris && Raoul drove

  Creates a new child process using process_execute() and returns
  the newly created child's pid. 
  
  Exits with -1 if cmdline pointer is illegal, or
  returns -1 if process_execute resulted in a failure. */
pid_t
exec(void *cmdline_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  const char *cmdline = *(const char **)cmdline_;

  check_string_validity(cmdline, 0);

  /* Call process_execute() so that the parent can create the new child process,
  and return the new pid (tid) */
  return process_execute(cmdline);
}

/* Raoul && Nicholai && Chris drove

  Blocks parents until child terminates, then returns child's exit status.*/
int 
wait(void *pid_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  pid_t pid = *(pid_t *)pid_;

  /* Call process_wait() so that the parent can wait on the child process and 
  return the child's exit status */
  return process_wait(pid);
}

/* Chris && Raoul drove 
  
  Creates a new file with given name and size and adds its file descriptor
  to running process' file descriptor array. 
  
  Exits with -1 if the file name is an illegal string, or returns
  false if filesys_create was unsuccessful. */
bool 
create(void *file_, void *initial_size_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  unsigned initial_size = *(unsigned *)initial_size_;
  const char *file = *(const char **)file_;

  check_string_validity(file, 0);

  /* Only 1 process is allowed to create a file at a time */
  lock_acquire(&file_lock);
  bool create = filesys_create(file, initial_size);
  lock_release(&file_lock);

  return create;
}

/* Raoul && Nicholai drove
  
  Removes given file from disk and deletes its file descriptor entry
  within the running process.
  
  Exits with -1 if the file name is an illegal string, or returns
  false if filesys_remove was unsuccessful. */
bool 
remove(void *file_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  const char *file = *(const char **)file_;

  check_string_validity(file, 0);
  
  /* Only 1 process is allowed to remove a file at a time*/
  lock_acquire(&file_lock);
  bool remove_file = filesys_remove(file);

  /* If removal of file from disk is successful, then remove from the 
    proccess's file descriptors array */
  if (remove_file)
  {
    remove_file_descriptor(file);
  }
  lock_release(&file_lock);

  return remove_file;
}

/* Raoul drove 

  Opens given file and returns the file descriptor index for the file.

  Exits with -1 if the file name is an illegal string, or returns
  -1 if filesys_open was unsuccessful, if there was no space in the fd array,
  or if the file_name was NULL.*/
int 
open(void *file_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  const char *file = *(const char **)file_;

  struct file *open_file;
  int fd = -1;

  check_string_validity(file, 0);

  /* If name is empty, then return -1 */
  if (*file == '\0')
    return fd;

  lock_acquire(&file_lock);

  /* Attempt to open file */
  open_file = filesys_open(file);

  /* If file exists, add it to fd array */
  if (open_file != NULL)
  {
    fd = add_file_descriptor(open_file);
  }

  lock_release(&file_lock);

  return fd;
}

/* Nicholai drove 

  Returns the size of a file given a file descriptor. */
int 
filesize(void *fd_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  int fd = *(int *)fd_;

  struct file *cur_file;
  int size = 0;

  lock_acquire(&file_lock);

  cur_file = get_file(fd);
  if (cur_file != NULL)
  {
    /* Check to make sure no other process is modifying the files 
     and then get the size in bytes of the file */
    size = (int)file_length(cur_file);
  }

  lock_release(&file_lock);

  return size;
}

/* Chris drove 

  Reads 'size' bytes from the file/keyboard. */
int 
read(void *fd_, void *buffer_, void *size_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  int fd = *(int *)fd_;
  unsigned size = *(unsigned *)size_;
  char *buffer = *(char **)buffer_;
  
  int bytes_read = 0, i = 0;
  struct file *cur_file = NULL;
  uint32_t *pd = thread_current()->pagedir;

  check_string_validity(buffer, size);

  /* Only allow one process to read from STDIN/file */
  lock_acquire(&file_lock);

  /* Reads input from keyboard and stores it in buffer */
  if (fd == 0)
  {
    while (i < size)
    {
      char c = (char)input_getc();
      buffer[i] = c;
      i++;
    }
  }

  /* Reads from file*/
  else
  {
    cur_file = get_file(fd);
    bytes_read = 0;

    if (cur_file != NULL)
    {
      bytes_read = file_read(cur_file, buffer, size);
    }
    else
    {
      bytes_read = -1;
    }
  }

  lock_release(&file_lock);

  return bytes_read;
}

/* Nicholai and Chris drove

  Writes the buffer to the console/file. */
int 
write(void *fd_, void *buffer_, void *size_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  int fd = *(int *)fd_;
  unsigned size = *(unsigned *)size_;
  const char *buffer = *(const char **)buffer_;

  unsigned size_cpy = size;
  uint32_t *pd = thread_current()->pagedir;

  check_string_validity(buffer, size);

  /* Only allow one process to write to console/file */
  lock_acquire(&file_lock);

  /* Writes to console */
  if (fd == 1)
  {
    char *cur_buffer = buffer;
    // Batch the printf statements in equal sizes
    while (size > 0)
    {
      int cur_batch_size = OUT_BATCH_SIZE;

      /* Output leftover size or batch size, whichever is greater.
       E.g. if buffer is 163, we output 100 -> 63 -> done */
      if (size < OUT_BATCH_SIZE)
        cur_batch_size = size;

      /* Print to console with cur_batch_size bytes of the buffer */
      putbuf(cur_buffer, cur_batch_size);

      /* Move the buffer forward each batch segment so we keep track
       of where we are, and decremeant size */
      cur_buffer += cur_batch_size;
      size -= cur_batch_size;
    }
  }

  /* Writes to file*/
  else
  {
    struct file *cur_file = get_file(fd);
    size_cpy = 0;

    /* Write when file exists */
    if (cur_file != NULL)
    {
      size_cpy = file_write(cur_file, buffer, size);
    }
    else
    {
      size_cpy = -1;
    }
  }

  lock_release(&file_lock);

  return size_cpy;
}

/* Chris drove 

  Changes the next read/write to start at 'position'th byte in the file. */
void 
seek(void *fd_, void *position_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  int fd = *(int *)fd_;
  unsigned position = *(unsigned *)position_;

  struct file *cur_file = get_file(fd);

  lock_acquire(&file_lock);

  file_seek(cur_file, position);

  lock_release(&file_lock);
}

/* Raoul drove 

  Returns the 'position'th byte of which the next read/write will start at. */
unsigned 
tell(void *fd_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  int fd = *(int *)fd_;

  unsigned position;
  struct file *cur_file = get_file(fd);

  lock_acquire(&file_lock);

  position = file_tell(cur_file);

  lock_release(&file_lock);

  return position;
}

/* Nicholai drove 

  Closes the file. */
void 
close(void *fd_)
{
  /* Cast the variable(s) into their appropriate data type(s) */
  int fd = *(int *)fd_;

  is_fd_in_range(fd);

  lock_acquire(&file_lock);

  struct file *cur_file = get_file(fd);
  if(cur_file == NULL)
  {
    custom_exit(-1);
    NOT_REACHED();
  }

  file_close(cur_file);
  remove_file_descriptor(cur_file);

  lock_release(&file_lock);
}

/* To be implemented in projects 3/4 */

// mapid_t
// mmap(int fd, void *addr)
// {
// }

// void munmap(mapid_t mapid)
// {
// }

// bool chdir(const char *dir)
// {
// }

// bool mkdir(const char *dir)
// {
// }

// bool readdir(int fd, char name[READDIR_MAX_LEN + 1])
// {
// }

// bool isdir(int fd)
// {
// }

// int inumber(int fd)
// {
// }

////////////////////////////////////////////////////////////////////////////////
/* Helper functions added by group */


/* Chris and Nicholai drove 

  Sequential search through an int array with length 'size' */
bool 
is_in_array(int value, int array[], int size)
{
  int i;
  for (i = 0; i < size; i++)
    if (array[i] == value)
      return true;

  return false;
}

/* Nicholai && Chris && Raoul drove

  Custom exit function which stores the exiting process's status, releases any
  resources it might be holding, closes its executable file, and dellocates
  itself.   */
void 
custom_exit(int status)
{
  struct thread *exiting_thread = thread_current();
  struct thread *parent_thread = exiting_thread->parent_thread;
  exiting_thread->exit_status = status;
  printf("%s: exit(%d)\n", exiting_thread->name, status);

  /* Release the file system lock if the thread holds it */
  if(lock_held_by_current_thread(&file_lock))
  {
    lock_release(&file_lock);
  }

  /* Close the ruuning executable file */
  if (thread_current()->running_exe != NULL)
    file_close(thread_current()->running_exe);

  /* Changing thread status to dying and schedule new thread */
  thread_exit();
  NOT_REACHED();
}

/* Nicholai drove 
  
  Calls custom_exit to send -1 failure status. */
void 
check_string_validity(char *s, int size)
{
  /* Get user's page directory */
  uint32_t *pd = thread_current()->pagedir;

  /* Check if the string pointer is valid */
  if(size == 0)
  {
    /* Insure that the pointer is legal */
    if(s == NULL || 
      !is_user_vaddr(s) || 
      pagedir_get_page(pd, s) == NULL)
    {
      custom_exit(-1);
      NOT_REACHED();
    }
  }
  /* Check the whole string pointer for read/write */
  else
  {
    /* Insure that the pointer is legal */
    if(s == NULL || 
      !is_user_vaddr(s) ||
      !is_user_vaddr(s+size) ||
      pagedir_get_page(pd, s) == NULL ||
      pagedir_get_page(pd, s+size) == NULL)
    {
      custom_exit(-1);
      NOT_REACHED();
    }
  }
}

/* Nicholai drove
  
  If fd is out of range, call custom_exit and send -1 failure status. */
void 
is_fd_in_range(int fd)
{
  if(fd < 2 || fd > MAX_FILE_DESCRIPTORS)
  {
    custom_exit(-1);
    NOT_REACHED();
  }
}