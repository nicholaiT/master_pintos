#include <syscall-nr.h>
#include <stdbool.h>
#include <debug.h> 


typedef int pid_t;

#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

void syscall_init(void);

/* Projects 2 and later. */
void halt(void) NO_RETURN;
void exit(void *status) NO_RETURN;
pid_t exec(void *file);
int wait(void *pid);
bool create(void *file, void *initial_size);
bool remove(void *file);
int open(void *file);
int filesize(void *fd);
int read(void *fd, void *buffer, void *length);
int write(void *fd, void *buffer, void *size);
void seek(void *fd, void *position);
unsigned tell(void *fd);
void close(void *fd);

// /* Project 3 and optionally project 4. */
// mapid_t mmap (int fd, void *addr);
// void munmap (mapid_t);

// /* Project 4 only. */
// bool chdir (const char *dir);
// bool mkdir (const char *dir);
// bool readdir (int fd, char name[READDIR_MAX_LEN + 1]);
// bool isdir (int fd);
// int inumber (int fd);

#endif /* userprog/syscall.h */
